# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=plougher ]

SUMMARY="The Squashfs tools for creating compressed filesystems"
DESCRIPTION="
Squashfs is a highly compressed read-only filesystem for Linux. It uses zlib to
compress files, inodes, and directories. All blocks are packed to minimize the
data overhead, and block sizes of between 4K and 1M are supported. It is intended
to be used for archival use, for live CDs, and for embedded systems where low
overhead is needed. This package allows you to create such filesystems.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    lz4 [[ description = [ Support lz4 compressed images ] ]]
    lzo [[ description = [ Support lzo compressed images ] ]]
    xz [[ description = [ Support xz compressed images ] ]]
    zstd [[ description = [ Support zstd compressed images ] ]]
"

DEPENDENCIES="
    build+run:
        lz4? ( app-arch/lz4 )
        lzo? ( app-arch/lzo:2 )
        xz? ( app-arch/xz )
        zstd? ( app-arch/zstd )
    run:
        sys-libs/zlib
"

WORK=${WORKBASE}/${PNV}/${PN}

DEFAULT_SRC_PREPARE_PATCHES=(
    -p2 "${FILES}"/f5c908e92d4c055859be2fddbda266d9e3bfd415.patch
)

DEFAULT_SRC_COMPILE_PARAMS=( CC="${CC}" LDFLAGS="${LDFLAGS}" )
DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_DIR="${IMAGE}"/usr/$(exhost --target)/bin )
DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( ../ )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( ACKNOWLEDGEMENTS RELEASE-READMEs/PERFORMANCE.README )

src_prepare() {
    default

    option lz4 && edo sed -i -e 's/#LZ4_SUPPORT = 1/LZ4_SUPPORT = 1/' Makefile
    option lzo && edo sed -i -e 's/#LZO_SUPPORT = 1/LZO_SUPPORT = 1/' Makefile
    option xz && edo sed -i -e 's/#XZ_SUPPORT = 1/XZ_SUPPORT = 1/' Makefile
    option zstd && edo sed -i -e 's/#ZSTD_SUPPORT = 1/ZSTD_SUPPORT = 1/' Makefile
}

